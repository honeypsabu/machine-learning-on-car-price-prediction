# Machine Learning on Car Price Prediction

## Overview
This project, presented by Honey Ponnanchery Sabu, Josh Jom, and Alex Dilsha, aims to predict automobile prices using machine learning techniques. Developed as part of our coursework in Computer Science (M.Sc.), this project encompasses exploratory data analysis, data cleaning, and the building of a predictive model.

## Dataset
The dataset named "auto_Price1" consists of 12,000 observations across 16 variables, including car name, seller type, price, and more. It has undergone thorough cleaning and preprocessing to ensure quality inputs for our model.

## Exploratory Data Analysis (EDA)
Data Collection & Cleaning: Initial steps involved collecting and cleaning the dataset to remove irrelevant variables and entries.
Multivariate Analysis: We employed correlation matrixes and encoding techniques to understand the relationships between variables.

## Model Building
Training: A Linear Regression model was chosen due to its suitability for our continuous numerical target variable, price.
Evaluation: Model performance was assessed using Root Mean Squared Error (RMSE) and r2_score, indicating high prediction accuracy.

## Conclusion
Our final model demonstrates the potential of linear regression in predicting auto prices with significant accuracy. This project not only showcases our analytical and modeling skills but also serves as a valuable tool for potential buyers and sellers in the auto industry.

## Contributors
Ponnanchery Sabu, Honey
Alex, Dilsha
Josh, Jom





